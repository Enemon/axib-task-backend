## Axib-task
Choose a self-explaining name for your project.

## Description
Simple flask app, that used as backend service and shows pong on /ping

## Installation

just make Installation section from frontend repository
https://gitlab.com/Enemon/axib-task-frontend

## Usage
### Run locally
Clone repository
`git clone https://gitlab.com/Enemon/axib-task-backend.git`

move to project directory
`cd ./axib-task-backend`

build docker image
`docker build -f .deploy/Dockerfile.local -t flask-app .`

Deploy docker stack (with local settings)
`docker stack deploy -c ./.deploy/docker-compose.yml -c ./.deploy/docker-compose-local.yml test-app`
docker-compose-local.yml contain settings for local development

move to http://localhost:5001/ping
if backend is up, you will see button with text "pong"

### Using CI/CD

First of all, make sure that you done all preparation steps on target server

Move to remoteapi certificates folder
`cd /opt/remoteapi/certs/client`

Now we need export those certs to Gitlab variables in base64
`cat ca.pem | base64 -w0`
`cat cert.pem | base64 -w0`
`cat key.pem | base64 -w0`

Add output of those three commands to separated group/repository variables

TLSCACERT_DEV_SERVER - `cat ca.pem | base64 -w0` 
TLSCERT_DEV_SERVER - `cat cert.pem | base64 -w0`
TLSKEY_DEV_SERVER - `cat key.pem | base64 -w0`

After we need change DOCKER_HOST variable in deploy step, to real server domain
```yaml
  image: docker:20.10.0
  variables:
    DOCKER_HOST: tcp://app.loc:2317
    DOCKER_TLS_VERIFY: 1
    DOCKER_CERT_PATH: "/certs"
  before_script:
    - *before_s
```
`tcp://...:2317` - TCP protocol and port must be defined here

Now we can commit and push in main branch, CI/CD will start automaticly (we could also make different rules for build/deploy on separated branches)
## Author
Nemokaiev Dmytro, @enemon
